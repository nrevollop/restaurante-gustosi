package com.example.restaurante;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.example.restaurante.baseDeDatos.table_pedido_producto;
import com.example.restaurante.databinding.FragmentFirstBinding;
import com.example.restaurante.entidades.model_producto_pedido;

import java.util.ArrayList;

public class FirstFragment extends Fragment {

    private FragmentFirstBinding binding;

    private Context thisContext;
    private ArrayList<model_producto_pedido> array_pedidos=new ArrayList<>();
    private int ID_USUARIO;
    private int ID_PEDIDO;
    private LinearLayout body_pedidos;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        thisContext=container.getContext();



        binding = FragmentFirstBinding.inflate(inflater, container, false);
        return binding.getRoot();

    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        body_pedidos = getView().findViewById(R.id.body_pedidos);
        llenarPedidos();
        binding.buttonFirst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(FirstFragment.this)
                        .navigate(R.id.action_FirstFragment_to_SecondFragment);
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    public void llenarPedidos(){
        Intent intent=getActivity().getIntent();
        Bundle bundle=intent.getExtras();
        if(bundle!=null){
            ID_USUARIO=bundle.getInt("id_usuario");
            ID_PEDIDO=bundle.getInt("id_pedido");
        }
        table_pedido_producto tpp=new table_pedido_producto(thisContext);
        array_pedidos=tpp.getNroPedido(ID_USUARIO,ID_PEDIDO);
        for (int i=0;i<array_pedidos.size();i++){
            productoPedido pp=new productoPedido(thisContext);
            model_producto_pedido mpp=array_pedidos.get(i);
            pp.setId(Integer.valueOf(mpp.getId_pedido()+""+mpp.getId_producto()));
            pp.setData(mpp.getNombre(),mpp.getDescripcion(),mpp.getTotal(),mpp.getCantidad(),mpp.getUrl_imagen(),mpp.getId_producto(),mpp.getId_pedido());
            body_pedidos.addView(pp);
        }
    }

}