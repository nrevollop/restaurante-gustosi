package com.example.restaurante;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.LocaleList;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.restaurante.baseDeDatos.DbData;
import com.example.restaurante.baseDeDatos.LlenarDatos;
import com.example.restaurante.baseDeDatos.table_categoria;
import com.example.restaurante.baseDeDatos.table_producto;
import com.example.restaurante.baseDeDatos.table_usuario;
import com.example.restaurante.entidades.usuario;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private String nombre;
    private String password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        try{
            DbData dbData=new DbData(MainActivity.this);
            SQLiteDatabase db=dbData.getWritableDatabase();
            if(db!=null){
                agregarProductoCategoria();
            }else{
                Toast.makeText(MainActivity.this,"Error: No se creo la base de datos",Toast.LENGTH_LONG).show();
            }
        }catch(Exception e){
            Toast.makeText(MainActivity.this,"Error: Al crear la base de datos",Toast.LENGTH_LONG).show();
        }
    }

    public void agregarProductoCategoria(){


        table_categoria tc=new table_categoria(this);
        if(tc.getCategorias().size()==0){
            LlenarDatos.llenarCategorias(this);
        }

        table_producto tp=new table_producto(this);
        if(tp.getProductos().size()==0){
            LlenarDatos.llenarProductos(this);
        }
    }

    public void ingresar_datos(View view){
        Intent intent= new Intent(MainActivity.this,ingresar_datos.class);
        startActivity(intent);
    }
    public void ingresar_gustosi(View view){
        TextView pant_nom=findViewById(R.id.inputusuario);
        nombre=pant_nom.getText().toString();
        TextView pant_pass=findViewById(R.id.inputcontraseña);
        password=pant_pass.getText().toString();

        table_usuario dbusuario=new table_usuario(this);
        ArrayList<usuario> listaUsuarios=dbusuario.mostrarUsuario(nombre);
        if(listaUsuarios.size()>0){
            usuario u=listaUsuarios.get(0);
            if(u.getPassword().equals(password)){
                if(u.getRol()==2){
                    Intent intent= new Intent(MainActivity.this,menu_gustosi.class);
                    intent.putExtra("id_usuario",u.getId_usuario());
                    startActivity(intent);
                }

            }else{
                Toast.makeText(MainActivity.this,"Contraseña no valida",Toast.LENGTH_LONG).show();
            }
        }else{
            Toast.makeText(MainActivity.this,"Usuario no valido",Toast.LENGTH_LONG).show();
        }

    }


}