package com.example.restaurante.baseDeDatos;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DbData extends SQLiteOpenHelper {
    public static final String DATABASE_NOMBRE="ProductosDb.db";
    public static final String TABLE_PRODUCTO="producto";
    public static final String TABLE_USUARIO="usuario";
    public static final String TABLE_PEDIDO="pedido";
    public static final String TABLE_PEDIDO_PRODUCTO="pedido_producto";
    public static final String TABLE_CATEGORIA="categoria";
    public static final int DATABASE_VERSION=1;



    public DbData(@Nullable Context context) {
        super(context, DATABASE_NOMBRE, null ,DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS "+TABLE_USUARIO+" ("+
                " id_usuario INTEGER PRIMARY KEY AUTOINCREMENT,"+
                " nombre TEXT NOT NULL,"+
                " password TEXT NOT NULL,"+
                " rol NUMERIC NOT NULL)" );

        db.execSQL("CREATE TABLE IF NOT EXISTS "+TABLE_PEDIDO+" ("+
                " id_pedido INTEGER PRIMARY KEY AUTOINCREMENT,"+
                " estado TEXT NOT NULL,"+
                " id_usuario INTEGER,"+
                " FOREIGN KEY(id_usuario) REFERENCES "+TABLE_USUARIO+"(id_usuario))" );

        db.execSQL("CREATE TABLE IF NOT EXISTS "+TABLE_CATEGORIA+" ("+
                " id_categoria INTEGER PRIMARY KEY AUTOINCREMENT,"+
                " nombre TEXT NOT NULL)" );

        db.execSQL("CREATE TABLE IF NOT EXISTS "+TABLE_PRODUCTO+" ("+
                " id_producto INTEGER PRIMARY KEY AUTOINCREMENT,"+
                " url_imagen TEXT,"+
                " nombre TEXT NOT NULL,"+
                " descripcion TEXT,"+
                " precio NUMERIC NOT NULL,"+
                " id_categoria NUMERIC NOT NULL,"+
                " FOREIGN KEY(id_categoria) REFERENCES "+TABLE_CATEGORIA+"(id_categoria))" );

        db.execSQL("CREATE TABLE IF NOT EXISTS "+TABLE_PEDIDO_PRODUCTO+" ("+
                " id_producto INTEGER NOT NULL,"+
                " id_pedido INTEGER NOT NULL,"+
                " cantidad INTEGER NOT NULL,"+
                " FOREIGN KEY(id_producto) REFERENCES "+TABLE_PRODUCTO+"(id_producto),"+
                " FOREIGN KEY(id_pedido) REFERENCES "+TABLE_PEDIDO+"(id_pedido))" );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
