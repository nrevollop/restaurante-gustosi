package com.example.restaurante.baseDeDatos;

import android.content.Context;

import com.example.restaurante.R;

public class LlenarDatos {
    public static void llenarCategorias(Context context){
        table_categoria t=new table_categoria(context);
        if(t.getCategorias().size()==0){
            t.agregarCategoria(1,"Comida rapida");//1
            t.agregarCategoria(2,"Comida nacional");//2
            t.agregarCategoria(3,"postres");//3
            t.agregarCategoria(4,"refrescos");//4
        }
    }

    public static void llenarProductos(Context context){
        table_producto t=new table_producto((context));
        if(t.getProductos().size()==0){
            t.agregarProducto(1,String.valueOf(R.mipmap.ic_launcher_foreground),"Hamburguesa doble",
                    "Doble carne de res, papas medianas y refresco gaseosa de 500ml",26.0,1);
            t.agregarProducto(2,String.valueOf(R.mipmap.pizza_foreground),"pizza",
                    "pizza tradicional mediana (dos porciones)",30.5,1);
            t.agregarProducto(3,String.valueOf(R.mipmap.tacos_foreground),"tacos",
                    "2 tacos de carne tradicional",20.0,1);
            t.agregarProducto(4,String.valueOf(R.mipmap.cuarto_de_pollo_foreground),"cuarto de pollo",
                    "2 presas de pollo con papas y arroz pequeño",32.0,1);
            t.agregarProducto(5,String.valueOf(R.mipmap.nuggets_foreground),"nuggets",
                    "seis nuggets con salsa tradicional de la casa",24.5,1);
            t.agregarProducto(6,String.valueOf(R.mipmap.saltenia_foreground),"salteña",
                    "2 tacos de carne tradicional",20.0,1);
            t.agregarProducto(7,String.valueOf(R.mipmap.hogdog_foreground),"hog dog",
                    "hog dog con sus salsas y repollo",6.0,1);
            t.agregarProducto(8,String.valueOf(R.mipmap.porcion_de_papas_foreground),"porcion de papas",
                    "una porcion de papas mediana con sus respectivas salsas",10.0,1);
            t.agregarProducto(9,String.valueOf(R.mipmap.ramen_foreground),"ramen",
                    "ramen mediano tradicional mixto",16.5,1);
            t.agregarProducto(10,String.valueOf(R.mipmap.tucumana_foreground),"tucumanas",
                    "una tucumana mixta",6.0,1);
            t.agregarProducto(21,String.valueOf(R.mipmap.charque_foreground),"charque orureño",
                    "charque mediano tradicional orureño",30.0,2);
            t.agregarProducto(22,String.valueOf(R.mipmap.chicharron_trucha_foreground),"chicharron de trucha",
                    "chicharron de trucha",42.0,2);
            t.agregarProducto(23,String.valueOf(R.mipmap.majadito_foreground),"majadito",
                    "majadito cruseño tradicional",25.0,2);
            t.agregarProducto(24,String.valueOf(R.mipmap.mondongo_foreground),"mondongo",
                    "mondongo tradicional",30.0,2);
            t.agregarProducto(25,String.valueOf(R.mipmap.pique_macho_foreground),"pique macho",
                    "pique macho mediano tradicional",45.0,2);
            t.agregarProducto(26,String.valueOf(R.mipmap.planchita_foreground),"planchita",
                    "planchita personal tradiional",60.0,2);
            t.agregarProducto(27,String.valueOf(R.mipmap.plato_pacenio_foreground),"plato paceño",
                    "",6.0,2);
            t.agregarProducto(28,String.valueOf(R.mipmap.saice_foreground),"saice",
                    "saice tradicional chuquisaqueño",36.0,2);
            t.agregarProducto(29,String.valueOf(R.mipmap.silpancho_foreground),"silpancho",
                    "silpancho cochabambino ",25,2);
            t.agregarProducto(30,String.valueOf(R.mipmap.sopa_de_mani_foreground),"sopa de mani",
                    "sopa de mani mediana",20.0,2);
            t.agregarProducto(31,String.valueOf(R.mipmap.brownie_foreground),"Brownie",
                    "una porcion de brownie de chocolate",15.0,3);
            t.agregarProducto(32,String.valueOf(R.mipmap.flan_foreground),"Flan",
                    "flan tradicional con su caramelo",12.0,3);
            t.agregarProducto(33,String.valueOf(R.mipmap.gelatina_foreground),"gelatina",
                    "gelatina de frutas mixtas envueltas en una sola capa de sabor ",10.0,3);
            t.agregarProducto(34,String.valueOf(R.mipmap.helado_foreground),"helado",
                    "tres porciones de helado de chocolate en copa ",18.0,3);
            t.agregarProducto(35,String.valueOf(R.mipmap.mofin_foreground),"mofin",
                    "mofin de chocolate decorado con crema de chocolate y chispas",14.0,3);
            t.agregarProducto(36,String.valueOf(R.mipmap.pie_de_limon_foreground),"pie de limon",
                    "una porcion de pie de limon ",17.0,3);
            t.agregarProducto(41,String.valueOf(R.mipmap.dos_litros_foreground),"Gaseosa",
                    "puedes escoger el sabor entre coca , fanta o sprite de 2 litros",25.0,4);
            t.agregarProducto(42,String.valueOf(R.mipmap.chicha_morada_foreground),"chicha morada",
                    "jarra de litro de chicha morada con hielo",15.0,4);
            t.agregarProducto(43,String.valueOf(R.mipmap.coca_cola_foreground),"coca cola",
                    "coca cola de 500ml fria o natural",7.0,4);
            t.agregarProducto(44,String.valueOf(R.mipmap.frutilla_foreground),"jugo de frutilla",
                    "una jarra de litro de frutilla con hielo",18.0,4);
            t.agregarProducto(45,String.valueOf(R.mipmap.jugo_valle_foreground),"jugo del valle",
                    "jugo del valle de 500ml fria o natural",8.0,4);
            t.agregarProducto(46,String.valueOf(R.mipmap.gaseosa_lata_foreground),"gaseosa en lata",
                    "gaseosa en lata de 490ml puedes escoger entre fanta , sprite o coca cola",10.0,4);
            t.agregarProducto(47,String.valueOf(R.mipmap.limonada_foreground),"limonada",
                    "una jarra de litro de limonada con hielo y hierva buena",18.0,4);
            t.agregarProducto(48,String.valueOf(R.mipmap.moconchinchi_foreground),"moconchinchi",
                    "una jarra de litro de moconchinchi con hielo",18.0,4);

        }
    }
}
