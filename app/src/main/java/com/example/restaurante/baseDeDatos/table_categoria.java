package com.example.restaurante.baseDeDatos;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import androidx.annotation.Nullable;

import com.example.restaurante.entidades.categoria;

import java.util.ArrayList;

public class table_categoria extends DbData{
    Context context;

    public table_categoria(@Nullable Context context) {
        super(context);
        this.context = context;
    }

    public long agregarCategoria(int id_categoria,String nombre){
        long id=0;
        try {
            DbData dbData = new DbData(context);
            SQLiteDatabase db = dbData.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put("id_categoria",id_categoria);
            values.put("nombre",nombre);

            id = db.insert(TABLE_CATEGORIA, null, values);
        } catch (Exception e) {
            System.out.println("Error:" + e);
            e.toString();
        }
        return id;
    }

    public ArrayList<categoria> getCategorias(){
        ArrayList<categoria> listaCategoria=new ArrayList<categoria>();
        DbData dbData = new DbData(context);
        SQLiteDatabase db = dbData.getWritableDatabase();
        categoria c=null;
        Cursor cursorCategorias=db.rawQuery("SELECT id_categoria,nombre FROM " + TABLE_CATEGORIA
                , null);
        if(cursorCategorias.moveToFirst()){
            do{
                c=new categoria();
                c.setId_categoria(cursorCategorias.getInt(0));
                c.setNombre(cursorCategorias.getString(1));
                listaCategoria.add(c);
            }while(cursorCategorias.moveToNext());
        }
        cursorCategorias.close();
        return listaCategoria;
    }
}
