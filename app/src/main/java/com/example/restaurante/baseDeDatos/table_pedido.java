package com.example.restaurante.baseDeDatos;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import androidx.annotation.Nullable;

import com.example.restaurante.entidades.Pedido;

import java.util.ArrayList;

public class table_pedido extends DbData{
    Context context;

    public table_pedido(@Nullable Context context) {
        super(context);
        this.context = context;
    }

    public long AgregarPedido(String estado,int id_usuario){
        long id=0;
        try {
            DbData dbData = new DbData(context);
            SQLiteDatabase db = dbData.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put("estado",estado);
            values.put("id_usuario",id_usuario);

            id = db.insert(TABLE_PEDIDO, null, values);
        } catch (Exception e) {
            System.out.println("Error:" + e);
            e.toString();
        }
        return id;
    }

    public ArrayList<Pedido> getPedidoByUser(int id_usuario){
        ArrayList<Pedido> listaPedidos=new ArrayList<>();
        DbData dbData=new DbData(context);
        SQLiteDatabase db = dbData.getWritableDatabase();
        Pedido pedido=null;
        Cursor cursorPedido=db.rawQuery("SELECT id_pedido,estado,id_usuario "+
                "FROM "+TABLE_PEDIDO+
                " WHERE id_usuario= "+id_usuario,null);
        if(cursorPedido.moveToFirst()){
            do{
                pedido=new Pedido();
                pedido.setId_pedido(cursorPedido.getInt(0));
                pedido.setEstado(cursorPedido.getString(1));
                pedido.setId_usuario(cursorPedido.getInt(2));
                listaPedidos.add(pedido);
            }while(cursorPedido.moveToNext());
        }
        return listaPedidos;
    }

    public ArrayList<Pedido> getPedidoByUserAndState(int id_usuario,String estado){
        ArrayList<Pedido> listaPedidos=new ArrayList<>();
        DbData dbData=new DbData(context);
        SQLiteDatabase db = dbData.getWritableDatabase();
        Pedido pedido=null;
        Cursor cursorPedido=db.rawQuery("SELECT id_pedido,estado,id_usuario "+
                "FROM "+TABLE_PEDIDO+
                " WHERE id_usuario= "+id_usuario+
                " AND estado= \""+estado+"\"",null);
        if(cursorPedido.moveToFirst()){
            do{
                pedido=new Pedido();
                pedido.setId_pedido(cursorPedido.getInt(0));
                pedido.setEstado(cursorPedido.getString(1));
                pedido.setId_usuario(cursorPedido.getInt(2));
                listaPedidos.add(pedido);
            }while(cursorPedido.moveToNext());
        }
        return listaPedidos;
    }

}
