package com.example.restaurante.baseDeDatos;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import androidx.annotation.Nullable;

import com.example.restaurante.entidades.model_producto_pedido;
import com.example.restaurante.entidades.pedido_producto;

import java.util.ArrayList;

public class table_pedido_producto extends DbData{
    Context context;

    public table_pedido_producto(@Nullable Context context) {
        super(context);
        this.context = context;
    }

    public long AgregarPedidoProducto(int id_pedido,int id_producto,int cantidad){
        long id=0;
        try {
            DbData dbData = new DbData(context);
            SQLiteDatabase db = dbData.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put("id_pedido",id_pedido);
            values.put("id_producto",id_producto);
            values.put("cantidad",cantidad);

            id = db.insert(TABLE_PEDIDO_PRODUCTO, null, values);
        } catch (Exception e) {
            System.out.println("Error:" + e);
            e.toString();
        }
        return id;
    }

    public ArrayList<model_producto_pedido> getNroPedido(int id_usuario,int id_pedido){
        ArrayList<model_producto_pedido> listapp=new ArrayList<>();
        DbData dbData=new DbData(context);
        SQLiteDatabase db=dbData.getWritableDatabase();
        Cursor cursorpp=db.rawQuery("SELECT pp.id_producto,pp.id_pedido,p.id_usuario,pr.url_imagen,pr.nombre"+
                ",pr.descripcion,pr.precio,pp.cantidad,(pr.precio*pp.cantidad) total"+
                " FROM "+TABLE_PEDIDO_PRODUCTO+" pp "+
                " INNER JOIN "+TABLE_PEDIDO+" p ON p.id_pedido="+id_pedido+" AND p.estado=\"espera\""+
                "INNER JOIN "+TABLE_PRODUCTO+" pr ON pr.id_producto==pp.id_producto "+
                "WHERE p.id_usuario="+id_usuario,null);
        if(cursorpp.moveToFirst()){
            do{
                model_producto_pedido mpp=new model_producto_pedido();
                mpp.setId_producto(cursorpp.getInt(0));
                mpp.setId_pedido(cursorpp.getInt(1));
                mpp.setId_usuario(cursorpp.getInt(2));
                mpp.setUrl_imagen(cursorpp.getString(3));
                mpp.setNombre(cursorpp.getString(4));
                mpp.setDescripcion(cursorpp.getString(5));
                mpp.setPrecio(cursorpp.getDouble(6));
                mpp.setCantidad(cursorpp.getInt(7));
                mpp.setTotal(cursorpp.getDouble(8));
                listapp.add(mpp);

            }while(cursorpp.moveToNext());
        }
        return listapp;
    }

}
