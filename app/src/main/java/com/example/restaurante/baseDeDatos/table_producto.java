package com.example.restaurante.baseDeDatos;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import androidx.annotation.Nullable;

import com.example.restaurante.entidades.Producto;
import com.example.restaurante.entidades.categoria;

import java.util.ArrayList;

public class table_producto extends DbData{
    Context context;

    public table_producto(@Nullable Context context) {
        super(context);
        this.context = context;
    }

    public long agregarProducto(int id_producto,String url_imagen,String nombre,
                                String descripcion,double precio,int id_categoria){
        long id=0;
        try {
            DbData dbData = new DbData(context);
            SQLiteDatabase db = dbData.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put("id_producto",id_producto);
            values.put("url_imagen",url_imagen);
            values.put("nombre",nombre);
            values.put("descripcion",descripcion);
            values.put("precio",precio);
            values.put("id_categoria",id_categoria);

            id = db.insert(TABLE_PRODUCTO, null, values);
        } catch (Exception e) {
            System.out.println("Error:" + e);
            e.toString();
        }
        return id;
    }

    public ArrayList<Producto> getProductos(){
        ArrayList<Producto> listaProducto=new ArrayList<Producto>();
        DbData dbData = new DbData(context);
        SQLiteDatabase db = dbData.getWritableDatabase();
        Producto producto=null;
        Cursor cursorProducto=db.rawQuery("SELECT id_producto,url_imagen,nombre,descripcion,precio,id_categoria FROM " + TABLE_PRODUCTO
                , null);
        if(cursorProducto.moveToFirst()){
            do{
                producto=new Producto();
                producto.setId_producto(cursorProducto.getInt(0));
                producto.setUrl_imagen(cursorProducto.getString(1));
                producto.setNombre(cursorProducto.getString(2));
                producto.setDescripcion(cursorProducto.getString(3));
                producto.setPrecio(cursorProducto.getInt(4));
                producto.setId_categoria(cursorProducto.getInt(5));
                listaProducto.add(producto);
            }while(cursorProducto.moveToNext());
        }
        cursorProducto.close();
        return listaProducto;
    }

    public ArrayList<Producto> getProductosByCategory(int idCategory){
        ArrayList<Producto> listaProducto=new ArrayList<Producto>();
        DbData dbData = new DbData(context);
        SQLiteDatabase db = dbData.getWritableDatabase();
        Producto producto=null;
        Cursor cursorProducto=db.rawQuery("SELECT id_producto,url_imagen,nombre,descripcion,precio,id_categoria FROM " + TABLE_PRODUCTO+
                " WHERE id_categoria= "+idCategory, null);
        if(cursorProducto.moveToFirst()){
            do{
                producto=new Producto();
                producto.setId_producto(cursorProducto.getInt(0));
                producto.setUrl_imagen(cursorProducto.getString(1));
                producto.setNombre(cursorProducto.getString(2));
                producto.setDescripcion(cursorProducto.getString(3));
                producto.setPrecio(cursorProducto.getInt(4));
                producto.setId_categoria(cursorProducto.getInt(5));
                listaProducto.add(producto);
            }while(cursorProducto.moveToNext());
        }
        cursorProducto.close();
        return listaProducto;
    }


}
