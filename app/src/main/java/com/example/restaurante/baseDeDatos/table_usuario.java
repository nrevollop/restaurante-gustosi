package com.example.restaurante.baseDeDatos;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import androidx.annotation.Nullable;

import com.example.restaurante.entidades.usuario;

import java.util.ArrayList;

public class table_usuario extends DbData {
    Context context;


    public table_usuario(@Nullable Context context) {
        super(context);
        this.context = context;
    }

    public long agregarUsuario(String nombre, String password, int rol) {
        long id = 0;
        try {
            DbData dbData = new DbData(context);
            SQLiteDatabase db = dbData.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put("nombre", nombre);
            values.put("password", password);
            values.put("rol", rol);//el rol 2 es para usuario, y el 1 para administrador

            id = db.insert(TABLE_USUARIO, null, values);
        } catch (Exception e) {
            System.out.println("Error:" + e);
            e.toString();
        }
        return id;
    }

    public ArrayList<usuario> mostrarUsuario(String nombre) {
        ArrayList<usuario> listaUsuarios = new ArrayList<usuario>();
        DbData dbData = new DbData(context);
        SQLiteDatabase db = dbData.getWritableDatabase();
        usuario u = null;
        Cursor cursorUsuarios = null;
        cursorUsuarios = db.rawQuery("SELECT id_usuario,nombre,password,rol FROM " + TABLE_USUARIO +
                " WHERE nombre=\"" + nombre + "\"", null);
        if (cursorUsuarios.moveToFirst()) {
            do {
                u = new usuario();
                u.setId_usuario(cursorUsuarios.getInt(0));
                u.setNombre(cursorUsuarios.getString(1));
                u.setPassword(cursorUsuarios.getString(2));
                u.setRol(cursorUsuarios.getInt(3));
                listaUsuarios.add(u);
            } while (cursorUsuarios.moveToNext());
        }
        cursorUsuarios.close();
        return listaUsuarios;
    }
}