package com.example.restaurante;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.restaurante.baseDeDatos.table_usuario;
import com.example.restaurante.entidades.usuario;

import java.util.ArrayList;

public class ingresar_datos extends AppCompatActivity {

    public String nombre;
    public String contra;
    public String rep_contra;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ingresar_datos);
    }

    public void recuperarDatos(){
        TextView pant_contra=findViewById(R.id.pant_contra);
        contra=pant_contra.getText().toString();
        TextView pant_rep_contra=findViewById(R.id.pant_rep_contra);
        rep_contra=pant_rep_contra.getText().toString();
        TextView pant_nombre=findViewById(R.id.pant_nombre);
        nombre=pant_nombre.getText().toString();
    }

    public boolean confirmarContra(){

        return (contra.equals(rep_contra)) && !contra.equals("") && !nombre.equalsIgnoreCase("");
    }

    public boolean existeUsuario(String nom){
        table_usuario t=new table_usuario(this);
        ArrayList<usuario> listUsrs=t.mostrarUsuario(nom);
        return listUsrs.size()>0;
    }

    public void guardarDatos(View view){
        recuperarDatos();
        if(existeUsuario(nombre)){
            Toast.makeText(ingresar_datos.this,"El nombre de usuario ya existe",Toast.LENGTH_LONG).show();
            return;
        }
        if(confirmarContra()){
            table_usuario dbUsuario=new table_usuario(this);
            dbUsuario.agregarUsuario(nombre,contra,2);
            dbUsuario.close();
            Toast.makeText(ingresar_datos.this,"Se ingreso correctamente",Toast.LENGTH_LONG).show();
            onBackPressed();
        }else{
            Toast.makeText(ingresar_datos.this,"Error: datos no validos",Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onBackPressed() {
        //this is only needed if you have specific things
        //that you want to do when the user presses the back button.
        /* your specific things...*/
        super.onBackPressed();
    }
}