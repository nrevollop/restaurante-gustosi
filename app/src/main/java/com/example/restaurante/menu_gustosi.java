package com.example.restaurante;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Menu;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.restaurante.baseDeDatos.table_pedido;
import com.example.restaurante.baseDeDatos.table_producto;
import com.example.restaurante.entidades.Pedido;
import com.example.restaurante.entidades.Producto;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.navigation.NavigationView;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;

import com.example.restaurante.databinding.ActivityMenuGustosiBinding;

import java.util.ArrayList;

public class menu_gustosi extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;
    private ActivityMenuGustosiBinding binding;
    private producto p;
    private ArrayList<producto> prods=new ArrayList<producto>();
    private LinearLayout contenidoMenu;
    private LayoutInflater inflater;
    private Context context;
    private int id_producto=0;
    private int id_usuario;
    private int id_pedido;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMenuGustosiBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        setSupportActionBar(binding.appBarMenuGustosi.toolbar);

        DrawerLayout drawer = binding.drawerLayout;
        NavigationView navigationView = binding.navView;
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow)
                .setOpenableLayout(drawer)
                .build();

        inflater=getLayoutInflater();
        contenidoMenu=findViewById(R.id.menu_linearLayout);
        GetPedidosEspera();

        llenar(1);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_gustosi, menu);
        return true;
    }

    public void GetPedidosEspera(){
        table_pedido tp=new table_pedido(this);
        Intent intent=getIntent();
        Bundle bundle=intent.getExtras();
        if(bundle!=null){
            id_usuario=bundle.getInt("id_usuario");
            ArrayList<Pedido> listaPedidos=tp.getPedidoByUserAndState(id_usuario,"espera");
            if(listaPedidos.size()==0){
                long id_pedido=tp.AgregarPedido("espera",id_usuario);
                System.out.println("id_pedido: "+id_pedido);
                listaPedidos=tp.getPedidoByUserAndState(id_usuario,"espera");
            }
            Pedido pedido= listaPedidos.get(0);
            id_pedido=pedido.getId_pedido();
        }

    }

    public void llenar(int id_categoria){
        table_producto tp=new table_producto(this);
        ArrayList<Producto> pp=tp.getProductosByCategory(id_categoria);
        for(int i=0;i<pp.size();i++){
            Producto pro=pp.get(i);
            p=new producto(this,null);
            p.setId(pro.getId_producto());
            p.setData(pro.getNombre(),pro.getDescripcion(),pro.getPrecio()+"",pro.getUrl_imagen(),pro.getId_producto(),id_pedido);
            try{
                prods.add(p);
            }catch (Exception e){
                System.out.println(e);
            }

            contenidoMenu.addView(p);
        }

    }
    public void eliminar_menu(){
        for(int i=0;i<prods.size();i++){
            prods.get(i).removeAllViews();

        }
        prods.clear();
    }

    public void llenar_comida_nacional(View view){
        eliminar_menu();
        llenar(2);
    }

    public void llenar_comida_rapida(View view){
        eliminar_menu();
        llenar(1);
    }

    public void llenar_postres(View view){
        eliminar_menu();
        llenar(3);
    }

    public void llenar_refrescos(View view){
        eliminar_menu();
        llenar(4);
    }

    public void verPedidos(View view){
        Intent intent=new Intent(menu_gustosi.this,VerPedidos.class);
        intent.putExtra("id_pedido",id_pedido);
        intent.putExtra("id_usuario",id_usuario);
        startActivity(intent);
    }

    public void recuperarCantidad(View view){
        try{
            producto p=prods.get(1);
            Toast toast=Toast.makeText(this,"Se agrego "+p.getCantidad(),Toast.LENGTH_LONG);
            toast.show();
        }catch (Exception e){
            System.out.println("error"+ e);
        }

    }
}