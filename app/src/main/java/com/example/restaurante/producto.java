package com.example.restaurante;

import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.example.restaurante.baseDeDatos.table_pedido_producto;

public class producto extends LinearLayout {

    private EditText inputCantidad;//para el input cantidad
    private ImageView image;//para la imagen
    private TextView Title;//para el titulo
    private TextView Description;// La descripcion del producto
    private TextView Price; //precio del producto
    private TextView ButtonAdd; //Boton para agregar a la lista de pedidos

    private int ID_PEDIDO;
    private int ID_PRODUCTO;

    private Context CONTEXT;

    public producto(Context context){
        this(context,null);
    }

    public producto(Context context, @Nullable AttributeSet attrs){
        super(context, attrs);
        CONTEXT=context;
        initView();
    }

    private void initView(){
        LayoutInflater.from(getContext()).inflate(R.layout.producto,this,true);
    }


    public void setId(int id){
        inputCantidad =findViewById(R.id.editTextNumber);
        int idgenerate=Integer.parseInt("11111"+id);
        inputCantidad.setId(idgenerate);

        image=findViewById(R.id.ImagenViewPedido);
        image.setId(Integer.parseInt("11112"+id));
        image.setBackground(null);
        String a=String.valueOf(R.mipmap.ic_launcher_foreground);
        image.setImageResource(Integer.parseInt(a));

        Title=findViewById(R.id.titleViewPedido);
        Title.setId(Integer.parseInt("11113"+id));

        Description=findViewById(R.id.descriptionViewPedido);
        Description.setId(Integer.parseInt("11114"+id));

        Price=findViewById(R.id.PrecioViewPedido);
        Price.setId(Integer.parseInt("11115"+id));

        ButtonAdd=findViewById(R.id.DeleteButtonPedido);
        ButtonAdd.setId(Integer.parseInt("11116"+id));

        ButtonAdd.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                String in=inputCantidad.getText().toString();
                if(!in.equalsIgnoreCase("")){
                    try{
                        table_pedido_producto tpp=new table_pedido_producto(CONTEXT);
                        tpp.AgregarPedidoProducto(ID_PEDIDO,ID_PRODUCTO,Integer.valueOf(in));
                        Toast.makeText(CONTEXT,"Se agrego correctamente",Toast.LENGTH_LONG).show();
                        inputCantidad.setText("");
                    }catch(Exception e){
                        System.out.println("Error en la clase producto: "+e);
                        Toast.makeText(CONTEXT,"Error al ingresar datos",Toast.LENGTH_LONG).show();
                    }

                }else{
                    Toast.makeText(CONTEXT,"Debe de rellenar el campo \"Cantidad\" correspondiente",Toast.LENGTH_LONG).show();
                }

            }
        });

    }

    public void setData(String title,String description,String price,String urlImage,int id_producto,int id_pedido){
        Title.setText(title);
        Description.setText(description);
        Price.setText("Precio: "+price+" Bs");
        image.setImageResource(Integer.parseInt(urlImage));

        ID_PRODUCTO=id_producto;
        ID_PEDIDO=id_pedido;
    }

    public String getCantidad(){
        return inputCantidad.getText().toString();
    }



}
