package com.example.restaurante;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;


public class productoPedido extends LinearLayout {
    private ImageView image;//para la imagen
    private TextView Title;//para el titulo
    private TextView Description;// La descripcion del producto
    private TextView Total;//total del producto
    private TextView Cantidad; //cantidad del producto
    private TextView ButtonRemove; //Boton para Eliminar un producto



    private int ID_PRODUCTO;
    private int ID_PEDIDO;

    public productoPedido(Context context){
        this(context,null);
    }

    public productoPedido(Context context, @Nullable AttributeSet attrs){
        super(context,attrs);
        initView();
    }

    private void initView(){
        LayoutInflater.from(getContext()).inflate(R.layout.producto_pedido,this,true);
    }

    public void setId(int id){
        image=findViewById(R.id.imagenViewPedido2);
        image.setId(Integer.parseInt("21112"+id));
        image.setBackground(null);
        String a=String.valueOf(R.mipmap.ic_launcher_foreground);
        image.setImageResource(Integer.parseInt(a));

        Title=findViewById(R.id.titleViewPedido2);
        Title.setId(Integer.parseInt("21113"+id));

        Description=findViewById(R.id.descriptionViewPedido2);
        Description.setId(Integer.parseInt("21114"+id));

        Total=findViewById(R.id.PrecioViewPedido2);
        Total.setId(Integer.parseInt("21115"+id));

        Cantidad=findViewById(R.id.textView12);
        Cantidad.setId(Integer.parseInt("21116"+id));

        ButtonRemove=findViewById(R.id.deleteButtonPedido2);
        ButtonRemove.setId(Integer.parseInt("21117"+id));
    }

    public void setData(String title,String description,double total,int cantidad,String urlImage,int id_producto,int id_pedido){
        Title.setText(title);
        Description.setText(description);
        Total.setText("El total es: "+String.valueOf(total)+" Bs");
        Cantidad.setText("Cantidad pedida es de "+cantidad);
        image.setImageResource(Integer.parseInt(urlImage));

        ID_PRODUCTO=id_producto;
        ID_PEDIDO=id_pedido;
    }

}
